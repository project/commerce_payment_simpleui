------------
Commerce Payment Simple UI
------------
Customers that have no knowledge of Rules UI will want a simple payment methods
configuration page.

The module enables a simplified version of Commerce Payment Methods. It provides
a "configure" link that takes the user to the Rule Action Enable payment method
configure form. It also disables a lot of links and information that might be
confusing for some users.
